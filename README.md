# saltstack-wordpress

Vagrant + SaltStack + WordPress 'hello world' demo.

## Overview

Using Vagrant, create a new Ubuntu virtual machine. Then, using Salt, install WordPress and all the requisite software (php, mysql, nginx, etc).

## Requirements

* OS X
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant 1.6.3+](http://www.vagrantup.com/downloads.html)

### Quickstart

After cloning this repo and installing the above requirements, from the root of the repository, simply run `vagrant up` in your terminal.

The first time you run this command, a fresh Ubuntu image is downloaded and provisioned. This can take awhile (between 10-15 minutes), depending on network speed.

After a few minutes, you should see a `Done!` message with a summary of what operations were performed on the machine.

Once complete, simply point your local browser at [http://localhost:5555](http://localhost:5555) to see a fresh WordPress install. Neat!

Poke around the admin [http://localhost:5555/wp-admin](http://localhost:5555/wp-admin) with `admin/password` username combo. 

To shut down the VM, simply run `vagrant halt`.
