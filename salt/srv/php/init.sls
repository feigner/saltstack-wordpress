# cli-only package as php5 has an apache dependency
php:
  pkg.installed:
    - pkgs:
      - php5-fpm
      - php5-cli
      - php5-mysql

# Needed for the wordpress importer plugin (installs php-xml)
#libapache2-mod-php5:
 #pkg:
  #- installed
