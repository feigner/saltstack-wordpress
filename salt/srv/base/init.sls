apt-get update:
  cmd:
    - run

basics:
  pkg.installed:
    - pkgs:
      - bash
      - vim
      - curl
      - unzip
      - build-essential
      - tree

{% if grains['environment'] == 'development': %}
dev-basics:
  pkg.installed:
    - pkgs:
      - git
{% endif %}

# set admin user perms
create-admin-user:
  user.present: 
    - name: vagrant
    - home: /home/vagrant
    - shell: /bin/bash
    - groups:
      - admin
      - sudo
      - www-data
