# use the nginx ppa instead of mainline ubuntu package
nginx-ppa:
  pkgrepo.managed:
    - ppa: nginx/stable
    - name: nginx

# install / run nginx service
nginx:
  pkg:
    - installed
    - name: nginx
  service.running:
    - require:
      - pkg: nginx
    - watch:
      - file: /etc/nginx/nginx.conf

# set master config
/etc/nginx/nginx.conf:
  file.managed:
    - source: salt://nginx/templates/nginx.conf
    - user: root
    - group: root
    - require:
      - pkg: nginx
    - template: jinja

# set group perms on www-data
/var/www/html:
 file.directory:
   - group: www-data
   - mode: 774
   - recurse:
     - group

# copy / symlink basic server config
/etc/nginx/sites-available/wordpress.conf:
  file.managed:
    - source: salt://nginx/templates/wordpress.conf

/etc/nginx/sites-enabled/wordpress.conf:
  file.symlink:
    - target: /etc/nginx/sites-available/wordpress.conf

# remove default server config
/etc/nginx/sites-enabled/default:
  file.absent

# restart service on config change
wordpress-nginx:
  service.running:
    - name: nginx
    - require:
      - pkg: nginx
    - watch:
      - file: /etc/nginx/sites-available/wordpress.conf
