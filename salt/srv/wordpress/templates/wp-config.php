<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'UuHx(1c.K-.SJ;K5{o0sbCAmv@pcMGi@FCNtE0F`8(n;H+(+Ks< D!J-or_|{J0#');
define('SECURE_AUTH_KEY',  ']$F52XG&kT>:k/9!tQ5k3msq=*>0|5yBGGi|fgBR0+dIK+E.K$ijD*Y7{<mYk~9F');
define('LOGGED_IN_KEY',    'bX?Sx6*K5CSEaa[<X,6EA3EW4{mfWvZM :oW-(iElZQTT&+&fZb<l-]YvEh_6=1l');
define('NONCE_KEY',        'fF_1%J[Dp`V]!1ny,(!2,o`$v:@NTU_B};|QCp=fEh+s!C6<!}|}h(>-sIp6U7xO');
define('AUTH_SALT',        '; ~*wOT=<jwxy|zO&3s2?Irqy*JgvJ0NK]?2~bc&0Vc{Gn,@Y-9P4iJZ*USsN+;$');
define('SECURE_AUTH_SALT', 'g1$}TMYRXP4AmE$Vz6A.}ZjB)7c1xche=}Bc_8q|a=}WVQW$e&(/T.Fbd2[PiNZq');
define('LOGGED_IN_SALT',   '+is8cM-<hA8KEGs<;4M|[E.Y -gpZv^eZbo4<G uWNj&+<:n`Pl?]jzJB5qXWGjT');
define('NONCE_SALT',       'AMlOm]LL*BWi@*JuEElRL$I*d4/rFVWlI~Y[xk5IQ?A$D[^F[~3v]EnK2/^MRWHO');


/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
