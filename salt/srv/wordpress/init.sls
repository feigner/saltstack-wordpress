# create wordpress db
wordpress_db:
 mysql_database.present:
  - name: wordpress
 mysql_user.present:
  - name: wordpress
  - password: password
 mysql_grants.present:
  - database: wordpress.*
  - grant: ALL PRIVILEGES
  - user: wordpress
  - host: '%'

# download / install wp-cli
get-wp-cli:
 cmd.run:
  - name: 'curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar &&
           chmod +x wp-cli.phar &&
           mv wp-cli.phar /usr/local/bin/wp'
  - cwd: /tmp
  - unless: '[ -e /usr/local/bin/wp ]'

# install wp source
install-wp:
  cmd.run:
    - name: 'wp core download'
    - cwd: /var/www/html
    - user: www-data
    - unless: '[ -e /var/www/html/wp-config.php ]'

# copy our wp config
/var/www/html/wp-config.php:
  file.managed:
    - source: salt://wordpress/templates/wp-config.php
    - user: www-data
    - group: www-data

# run wp installer via cli
install_wordpress:
 cmd.run:
  - name: '/usr/local/bin/wp core install --url=http://localhost:5555 --title=development --admin_user=admin --admin_password=password --admin_email=fake@false.inv'
  - cwd: /var/www/html
  - user: www-data


