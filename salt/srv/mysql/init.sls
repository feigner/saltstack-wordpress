mysql-server:
 pkg:
   - installed
 service.running:
   - require:
     - pkg: mysql-server
   - names:
     - mysql

mysql-client:
  pkg:
    - installed

# additional mysql-related libs
mysql-libs:
  pkg.installed:
    - pkgs:
      - python-dev # let salt talk to mysql
      - python-mysqldb
      - libmysqlclient-dev
